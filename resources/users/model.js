'use strict';

const debug     = require('debug')('users');
const Boom      = require('boom');
const processId = require('../../lib/processId');
const Bluebird  = require('bluebird');
const bcrypt    = Bluebird.promisifyAll(require('bcrypt'));
const jwtSign   = require('jsonwebtoken').sign;
const RecordID  = require('orientjs/lib/recordid');

Object.assign = require('object-assign');

module.exports = function (cfg) {
  const db = cfg.chaptersdb;

  let model = {};

  function getIdentity(username) {
    return db
      .class
      .get('Identity')
      .call('find', { username })
      .then(function (records) {
        if(!records.length) {
          return null;
        }

        return records[0];
      });
  }

  model.add = function (username, email, password) {
    debug('creating %s', username);

    return getIdentity(username)
      .tap(function (record) {
        if(record) {
          throw Boom.conflict('User already exists');
        }
      })
      .then(bcrypt.genSaltAsync.bind(null, 10))
      .then(bcrypt.hashAsync.bind(null, password))
      .then(function (hash) {
        return db
          .class
          .get('Identity')
          .call('create', {
            username,
            email,
            hash
          });
      })
      .then(function (user) {
        debug('Succesfully created: %s', user.username);

        return {
          username
        };
      });
  };

  model.update = function (username, delta) {
    debug('updating: %s', username);

    return getIdentity(username)
      .then(function (record) {
        if(!record) {
          throw Boom.notFound('User does not exist');
        }

        if(record.username !== username) {
          throw Boom.unauthorized('Cannot edit another user!!!!');
        }

        return db
          .update(record['@rid'])
          .set(delta)
          .one()
          .thenReturn(record)
      })
      .then(processId)
      .tap(function() {
        debug('successfully updated %s', username);
      });
  };

  model.destroy = function (username) {
    debug('removing: %s', username);

    return getIdentity(username)
      .then(function (record) {
        if(!record) {
          throw Boom.notFound('User does not exist');
        }

        if(record.username !== username) {
          throw Boom.unauthorized('Cannot remove another user!');
        }

        return db
          .record
          .delete(record)
          .thenReturn(processId(record));
      });
  };


  //
  // XXX: Really this RecordID shit is bad method of telling whether we want a persona.
  //
  //      It used to be because they were in couch, they are no longer.
  //
  //      Couple of ideas:
  //
  //      a. Try for a user first, then check for a persona. (issue is username lookup…)
  //      b. Rip personas out of this file. Keep it for Identities.
  //
  //      YOU decide.
  //

  model.get = function (username) {
    debug('getting: %s', username);

    if(RecordID.isValid('#' + username)) {
      const id = '#' + username;
      return db
        .select()
        .from(id)
        .one()
        .then(processId)
        .then(doc => ({
          id:     doc.id,
          name:   doc.title,
          bio:    doc.description,
          author: doc.author
        }));
    }
    else {
      return getIdentity(username)
        .tap(function (record) {
          if(!record) {
            throw Boom.notFound('User not found');
          }
        });
    }
  };

  model.getStories = function (username, userToList) {
    debug('%s getting chapters for %s', username, userToList);

    if(RecordID.isValid('#' + userToList)) {
      // We have a persona
      return db
        .select("expand( out('Wrote') )")
        .from('#' + userToList)
        .fetch('*:1')
        .all()
        .map(processId)
        .map(function (doc) {
          return {
            id:          doc.id,
            title:       doc.title,
            description: doc.description,
            author:      doc.author
          };
        });
    }
    else {
      // We have an Identity
      return db
        .select("expand( out('Wrote') )")
        .from('Persona')
        .where("username='" + userToList + "'")
        .fetch('*:1')
        .all()
        .map(processId)
        .map(function (doc) {
          return {
            id:          doc.id,
            title:       doc.title,
            description: doc.description,
            author:      userToList
          };
        });
    }

  };

  model.getPersonas = function (username, userToList) {
    debug('%s getting chapters for %s', username, userToList);

    if(RecordID.isValid('#' + userToList)) {
      return db
        .select("expand( out('Wrote') )")
        .from('#' + userToList)
        .fetch('*:1')
        .all()
        .filter(doc => doc['@class'] === 'Persona')
        .map(processId)
        .map(function (doc) {
          debug(doc)
          return {
            id:          doc.id,
            title:       doc.title,
            description: doc.description
          };
        });
    }
    else {
      return db
        .select()
        .from('Persona')
        .where(`'${userToList}' IN in('Owns').username`)
        .all()
        .map(processId)
        .map(function (doc) {
          return {
            id:          doc.id,
            title:       doc.title,
            description: doc.description
          };
        });
    }

  };

  model.list = function () {
    return db
      .class
      .get('Identity')
      .call('list')
      .map(processId);
  };

  model.getToken = function (username, password) {
    debug('getting token for %s', username);

    return db
      .class
      .get('Identity')
      .call('find', { username })
      .then(function (records) {
        if(!records.length) {
          throw Boom.notFound('User does not exist');
        }

        const record = records[0];

        return bcrypt
          .compareAsync(password, record.hash)
          .thenReturn(record)
          .catch(function () {
            throw Boom.unauthorized('Wrong password');
          });
      })
      .then(function (record) {
        return jwtSign({ user: record.username }, cfg.tokenSecret, { expiresIn: cfg.tokenExpiration });
      });
  };

  return model;
};
