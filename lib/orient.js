// Connect to orient

// XXX Obviously this config should change
module.exports = require('orientjs')({
  host: process.env['CHAPTERS_ORIENTDB_HOST'] || 'localhost',
  port: process.env['CHAPTERS_ORIENTDB_PORT'] || 2424,
  username: 'root',
  password: process.env['ORIENTDB_ROOT_PASSWORD'] || 'rootpwd'
});
