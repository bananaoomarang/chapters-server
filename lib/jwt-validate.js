'use strict';

module.exports = function (cfg) {
  const db = cfg.chaptersdb;

  function verify (request, decoded, cb) {
    const key = decoded.user;

    db
      .class
      .get('Identity')
      .call('find', { username: key })
      .then(function (records) {
        if(!records.length) {
          return cb(new Error('User not found'), false, null);
        }

        return cb(null, true, { username: records[0].username, email: records[0].email });
      });
  }

  return verify;
};
