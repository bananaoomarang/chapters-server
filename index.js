'use strict';

const Hapi       = require('hapi');
const async      = require('async');
const Bluebird   = require('bluebird');

const COUCH_URL  = 'http://' + [process.env['CHAPTERS_COUCHDB_HOST'] || 'localhost', process.env['CHAPTERS_COUCHDB_POSRT'] || 5984].join(':');
const usersdb    = require('nano')(COUCH_URL + '/_users');

const orientServer = require('./lib/orient');

const chaptersdb = orientServer.use('chapters');

const server = module.exports = new Hapi.Server();

const cfg = {
  server:          server,
  usersdb:         Bluebird.promisifyAll(usersdb),
  chaptersdb:      chaptersdb,
  tokenSecret:     'QIrNlsLVNbaWbUILlGBdrwwLuiPah9IvYOFQcwcoh66sMOQ47v7jl44oxzd+a1shxVc+3MFlr+kjQs5O/zZvFDPDuGY04hFYUyJMgDcWYTb6ZycIV155OXSqdOvF5mN+hSh/02sMpVnZrWmlhEzwKeUyxaH0YNlEFhDnsv92l20=', // XXX Replace with secret for JWT validation
  tokenExpiration: '60m' // Token expiration timeout in minutes
};

const routes  = require('./lib/routes')(cfg);
const plugins = require('./lib/plugins')(cfg);

server.connection({ port: 8888 });

server.register(plugins.definitions, function next (err) {
  if(err)
    throw err;

  async.parallel(plugins.jobsPostLoad);
});

server.route(routes);
